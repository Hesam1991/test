import React from 'react';
import DisplayModal from './DisplayModal.js';

import { makeStyles } from '@material-ui/core/styles';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import Paper from '@material-ui/core/Paper';

const useStyles = makeStyles(theme => ({
    root: {
        width: '100%',
        marginTop: theme.spacing(3),
        overflowX: 'auto',
    },
    table: {
        minWidth: 650,
    },
    button: {
        margin: theme.spacing(1),
    },
}));

const InfTable = ({ inf }) => {
    const classes = useStyles();



    return (
        <Paper className={classes.root}>
            <Table className={classes.table}>
                <TableHead>
                    <TableRow>
                        <TableCell>Name</TableCell>
                        <TableCell align="center">Username</TableCell>
                        <TableCell align="center">Email</TableCell>
                        <TableCell align="center">Detail</TableCell>
                    </TableRow>
                </TableHead>
                <TableBody>
                    {inf.map(user => {

                        return (
                            <TableRow key={user.id}>
                                <TableCell>{user.name}</TableCell>
                                <TableCell align="center">{user.username}</TableCell>
                                <TableCell align="center">{user.email}</TableCell>
                                <TableCell align="center">
                                    <DisplayModal data={user}/>
                                </TableCell>
                            </TableRow>
                        )
                    })
                    }

                </TableBody>
            </Table>
        </Paper>
    );
}

export default InfTable;
