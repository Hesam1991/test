import React from 'react';
import Modal from 'react-modal';
import UserInfo from './UserInfo.js';

import Button from '@material-ui/core/Button';

Modal.setAppElement('#root')

class DisplayModal extends React.Component {
    constructor() {
        super();
        this.state = {
            showModal: false
        };

        this.handleOpenModal = this.handleOpenModal.bind(this);
        this.handleCloseModal = this.handleCloseModal.bind(this);
    }

    handleOpenModal() {
        this.setState({ showModal: true });
    }

    handleCloseModal() {
        this.setState({ showModal: false });
    }

    render() {
        const {data} = this.props; 
        console.log(data);
        
        return (
            <React.Fragment>
                <Button variant="contained"
                    color="primary"
                    onClick={this.handleOpenModal}
                >
                    More
                </Button>
                <Modal
                    isOpen={this.state.showModal}
                >
                    <div className="closeButton">
                        <Button variant="contained"
                            color="secondary"
                            onClick={this.handleCloseModal}
                            >
                            Close
                        </Button>
                    </div>
                    <UserInfo data={data}/>
                </Modal>

            </React.Fragment>
        );
    }
}
export default DisplayModal;
