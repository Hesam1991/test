import React from 'react';
import { Map, GoogleApiWrapper, Marker } from 'google-maps-react';



class MyMap extends React.Component {
  constructor(props) {
    super(props);

    this.state = {}
  }
  
  displayMarkers = () =>  <Marker position={{lat: this.props.geo.lat, lng: this.props.geo.lng}} onClick={() => console.log("You clicked me!")} /> 

  render() {
    const mapStyles = {
      width: '75%',
      height: '75%',
      margin: '0 auto',
      marginTop: '30px' 
    };

    return (
    
        <Map
        google={this.props.google}
        zoom={8}
        style={mapStyles}
            initialCenter={{ lat: this.props.geo.lat, lng: this.props.geo.lng}}
        >
        {this.displayMarkers()}
            </Map>
       
       
          
    );
  }
}
export default GoogleApiWrapper({
  apiKey: 'AIzaSyC2pzshErpdm1D84bJggaqne11tKr3VQaM'
})(MyMap);

