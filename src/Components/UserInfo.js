import React from 'react';
import MyMap from './MyMap.js';

import { withStyles, makeStyles } from '@material-ui/core/styles';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import Paper from '@material-ui/core/Paper';

const StyledTableCell = withStyles(theme => ({
    head: {
        backgroundColor: theme.palette.common.black,
        color: theme.palette.common.white,
    },
    body: {
        fontSize: 14,
    },
}))(TableCell);

const StyledTableRow = withStyles(theme => ({
    root: {
        '&:nth-of-type(odd)': {
            backgroundColor: theme.palette.background.default,
        },
    },
}))(TableRow);

const useStyles = makeStyles(theme => ({
    root: {
        width: '100%',
        marginTop: theme.spacing(3),
        overflowX: 'auto',
    },
    table: {
        minWidth: 700,
    },
}));


const UserInfo = ({ data }) => {
    const classes = useStyles();



    return (
        <Paper className={classes.root}>
            <Table className={classes.table}>
                <TableHead>
                    <TableRow>
                        <StyledTableCell>Personal Information</StyledTableCell>
                        <StyledTableCell></StyledTableCell>
                        <StyledTableCell></StyledTableCell>
                        <StyledTableCell></StyledTableCell>
                        <StyledTableCell></StyledTableCell>
                    </TableRow>
                    <TableRow>
                        <TableCell>Name</TableCell>
                        <TableCell align="center">Username</TableCell>
                        <TableCell align="center">Email</TableCell>
                        <TableCell align="center">Website</TableCell>
                        <TableCell align="center">Phone</TableCell>
                    </TableRow>
                </TableHead>
                <TableBody>

                    <StyledTableRow>
                        <StyledTableCell component="th" scope="row">{data.name}</StyledTableCell>
                        <StyledTableCell align="center">{data.username}</StyledTableCell>
                        <StyledTableCell align="center">{data.email}</StyledTableCell>
                        <StyledTableCell align="center">{data.website}</StyledTableCell>
                        <StyledTableCell align="center">{data.phone}</StyledTableCell>
                    </StyledTableRow>
                  

                </TableBody>

                <TableHead>
                    <TableRow>
                        <StyledTableCell>Company Information</StyledTableCell>
                        <StyledTableCell></StyledTableCell>
                        <StyledTableCell></StyledTableCell>
                        <StyledTableCell></StyledTableCell>
                        <StyledTableCell></StyledTableCell>
                    </TableRow>
                    <TableRow>
                        <TableCell>Name</TableCell>
                        <TableCell align="center">Catch Phrase</TableCell>
                        <TableCell align="center">Bs</TableCell>
                        <TableCell align="center"></TableCell>
                        <TableCell align="center"></TableCell>
                    </TableRow>
                </TableHead>
                <TableBody>

                    <StyledTableRow>
                        <StyledTableCell component="th" scope="row">{data.company.name}</StyledTableCell>
                        <StyledTableCell align="center">{data.company.catchPhrase}</StyledTableCell>
                        <StyledTableCell align="center">{data.company.bs}</StyledTableCell>
                        <StyledTableCell align="center"></StyledTableCell>
                        <StyledTableCell align="center"></StyledTableCell>
                    </StyledTableRow>


                </TableBody>
                <TableHead>
                    <TableRow>
                        <StyledTableCell>Location</StyledTableCell>
                        <StyledTableCell></StyledTableCell>
                        <StyledTableCell></StyledTableCell>
                        <StyledTableCell></StyledTableCell>
                        <StyledTableCell></StyledTableCell>
                    </TableRow>
                    <TableRow>
                        <TableCell>Adress</TableCell>
                        <TableCell align="center">Zipcode</TableCell>
                        <TableCell align="center"></TableCell>
                        <TableCell align="center"></TableCell>
                        <TableCell align="center"></TableCell>
                    </TableRow>
                </TableHead>
                <TableBody>

                    <StyledTableRow>
                        <StyledTableCell component="th" scope="row">
                            {`${data.address.suite}, ${data.address.street}, ${data.address.city}`}
                        </StyledTableCell>
                        <StyledTableCell align="center">{data.address.zipcode}</StyledTableCell>
                        <StyledTableCell align="center"></StyledTableCell>
                        <StyledTableCell align="center"></StyledTableCell>
                        <StyledTableCell align="center"></StyledTableCell>
                    </StyledTableRow>
                    <StyledTableRow>
                        <MyMap geo={data.address.geo}/>
                    </StyledTableRow>


                </TableBody>

                
            </Table>
        </Paper>



    );
}
export default UserInfo;