import React from 'react';
import InfTable from './InfTable.js';
import '../Style.css';


class App extends React.Component{
  state ={
    usersData: []
  }

  componentDidMount(){
    fetch('http://jsonplaceholder.typicode.com/users')
    .then(response => response.json())
    .then(data => this.setState({usersData : data}))
    .catch(err => console.error(err))
  }
  
  
  
  render(){
    return(
        <InfTable inf={this.state.usersData}/>
    );

  }



}
export default App;